const express = require("express");
const bcrypt = require("bcrypt");
const app = express();
const port = 3000;
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
var users = [
  {
    id: 1,
    username: "sara",
    password: "123456",
    firstname: "name1",
    lastname: "lname1",
  },
  {
    id: 2,
    username: "kami",
    password: "123456",
    firstname: "name2",
    lastname: "lname2",
  },
  {
    id: 3,
    username: "vida",
    password: "123456",
    firstname: "name3",
    lastname: "lname3",
  },
  {
    id: 4,
    username: "atila",
    password: "123456",
    firstname: "name4",
    lastname: "lname4",
  },
  {
    id: 5,
    username: "sina",
    password: "123456",
    firstname: "name5",
    lastname: "lname5",
  },
  {
    id: 6,
    username: "arash",
    password: "123456",
    firstname: "name6",
    lastname: "lname6",
  },
];
var services = [
  {
    id: 1,
    name: "service1",
    price: 100000,
  },
  {
    id: 2,
    name: "service2",
    price: 100000,
  },
  {
    id: 3,
    name: "service3",
    price: 100000,
  },
  {
    id: 4,
    name: "service4",
    price: 100000,
  },
  {
    id: 5,
    name: "service5",
    price: 100000,
  },
  {
    id: 6,
    name: "service6",
    price: 100000,
  },
];
var idUser = 6;
var idService = 6;
//////* User
//! Create
app.post("/users", async function (req, res) {
  let data = req.body;
  let newUser = {};
  if (findDuplicate(users, "username", data.username) !== false) {
    return res.status(406).json({
      error_code: 406,
      error_message: "Bad Request: Username Is Duplicate",
    });
  }
  newUser.id = ++idUser;
  newUser.password = await hashPassword(password);
  newUser.username = data.username;
  newUser.firstname = data.firstname;
  newUser.lastname = data.lasttname;
  users.push(newUser);
  return res.json(users[users.length - 1]);
});
//! Read
app.get("/users", function (req, res) {
  return res.send(users);
});
//! Update
app.put("/users/:id", function (req, res) {
  let id_req = parseInt(req.params.id);
  let data = req.body;
  // let item=findDuplicate(users, "id", id_req);
  let result = findDuplicate(users, "id", id_req);
  let item = result.item;
  if (!item) {
    return res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
  if (findDuplicate(users, "username", data.username, id_req) !== false) {
    return res.status(400).json({
      error_code: 400,
      error_message: "Bad Request: Username Is Duplicate",
    });
  }
  item.username = data.username;
  item.password = data.password;
  item.firstname = data.firstname;
  item.lastname = data.lasttname;

  res.json(item);
});
//! Delete
app.delete("/users/:id", function (req, res) {
  let id = parseInt(req.params.id);
  let result = findDuplicate(users, "id", id);
  if (result) {
    temp = result.item;
    users.splice(result.index, 1);
    flag = true;
    return res.json(temp);
  } else {
    return res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});
//////////* Service
//! Create
app.post("/services", function (req, res) {
  services.push({
    id: ++idService,
    name: req.body.name,
    age: req.body.price,
  });
  return res.json(services[services.length - 1]);
});
//! Read
app.get("/services", (req, res) => res.send(services));
//! Update
app.put("/services/:id", function (req, res) {
  let id = parseInt(req.params.id);
  let result = findDuplicate(services, "id", id);
  let item = result.item;
  if (!item) {
    return res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
  item.name = req.body.name;
  item.price = req.body.price;
  return res.json(item);
});
//! Delete
app.delete("/services/:id", function (req, res) {
  let id = parseInt(req.params.id);
  let temp;
  let result = findDuplicate(services, "id", id);
  if (result) {
    temp = result.item;
    services.splice(result.index, 1);
    flag = true;
    return res.json(temp);
  } else {
    return res.status(404).json({
      error_code: 404,
      error_message: "Not Found",
    });
  }
});

////////////? Functions
function findDuplicate(inputArray, key, targetValue, thisId = null) {
  let flg = {};
  inputArray.forEach(function (item, index) {
    if (item[key] === targetValue) {
      if (thisId !== null && item.id != thisId) {
        flg.item = item;
        flg.index = index;
        return;
      } else if (thisId === null) {
        flg.item = item;
        flg.index = index;
        return;
      }
    }
  });
  if (Object.keys(flg).length === 0) return false;
  return flg;
}
async function hashPassword(password) {
  const saltRounds = 10;

  const hashedPassword = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltRounds, function (err, hash) {
      if (err) reject(err);
      resolve(hash);
    });
  });

  return hashedPassword;
}
